package de.hska.lat.sphero.tracking;

import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
import static com.googlecode.javacv.cpp.opencv_core.cvCircle;
import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
import static com.googlecode.javacv.cpp.opencv_core.cvFlip;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
import static com.googlecode.javacv.cpp.opencv_core.cvInRangeS;
import static com.googlecode.javacv.cpp.opencv_core.cvScalar;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2RGB;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_MEDIAN;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvEqualizeHist;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvGetCentralMoment;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvGetSpatialMoment;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvMoments;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvSmooth;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.cpp.opencv_core.CvPoint;
import com.googlecode.javacv.cpp.opencv_core.CvScalar;
// load files for openCV framework.
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_imgproc.CvMoments;

import de.hska.lat.sphero.network.IKinectServerCallBack;
import de.hska.lat.sphero.network.impl.KinectServer;
import de.hska.lat.sphero.network.impl.objects.StringContainer;
// load files for openCV JNI framework.
// load files for utility functions.
// load files for Kinect framework
// load files for gui interface.

// load files for Roboview framework.

/**
 * Class for the detection of the Sphero robot. This class was created as part
 * of the lecture "Autonomous systems". It uses the Roboview framework for
 * controlling the Sphero, openCV for the detection of the Sphero and openNI for
 * the videocapture with the kinect.
 *
 * The goal of this project was to detect the sphero and send it to a specific
 * location in the real world, by pointing at it. The kinect is used to track a
 * person of interest. This is done by another student group. This project
 * focuses on the detection of the Sphero and moving it to a specific target.
 *
 * @author hoth1011
 * @author repa1013
 * @author roge1011
 * @version 1.0
 *
 */
public class SpheroTrackingNetwork implements IKinectServerCallBack
{
	// Caution, this will not work on your computer, you have to change
	// the file path to your local openNI library. At the moment the
	// Roboview framework does not support the kinect. Remebter that
	// you have to escape the backslashes.
	private static final int JITTER_TOLERANCE = 40;
	private static final int JITTER_BUFFER_LENGTH = 3;
	private static final int MIN = 0;
	private static final int MAX = 255;
	private static final int RGB_MIN = 250;
	private static final int RGB_MAX = 255;
	private static CvScalar rgba_min = cvScalar(RGB_MIN, RGB_MIN, RGB_MIN, 0);
	private static CvScalar rgba_max = cvScalar(RGB_MAX, RGB_MAX, RGB_MAX, 0);
	private int[] jitterBufferX = new int[JITTER_BUFFER_LENGTH];
	private int[] jitterBufferY = new int[JITTER_BUFFER_LENGTH];
	private int bufferPosition = 0;
	private boolean isInitial = true;
	private IplImage videoFrame;
	private CvPoint spheroCoordinates;
	private CanvasFrame output;
	private KinectServer kinectServer;
	private CanvasFrame rgbAdjustCanvasFrame;
	private JPanel mainPanel;
	private JPanel rgbAdjustPanel;
	private JSlider rSliderMin;
	private JSlider gSliderMin;
	private JSlider bSliderMin;
	private JSlider rSliderMax;
	private JSlider gSliderMax;
	private JSlider bSliderMax;
	private JLabel rLabelMin;
	private JLabel gLabelMin;
	private JLabel bLabelMin;
	private JLabel rLabelMax;
	private JLabel gLabelMax;
	private JLabel bLabelMax;

	private JLabel speedTextLabel;
	private JLabel speedLabel;
	private int imageCounter;
	private String currentPosition;
	private String oldPosition;
	private long currentTime;
	private long oldTime;

	/**
	 * Constructor for the Sphero tracking. Creates the Roboview interface for
	 * the "Sphero" robot. Initializes the openNI interface for the kinect.
	 * Makes two windows to display the captured and processed images from said
	 * webcam.
	 */
	public SpheroTrackingNetwork()
	{
		currentPosition=null;
		oldPosition=null;
		imageCounter=0;
		output = new CanvasFrame("RoboView - WORKING-ON video output");
		output.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		rgbAdjustCanvasFrame = new CanvasFrame("RoboView - RGB adjust");
		rgbAdjustCanvasFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainPanel = new JPanel(new BorderLayout());
		rgbAdjustCanvasFrame.setContentPane(mainPanel);

		rSliderMin = new JSlider(MIN, MAX, RGB_MIN);
		gSliderMin = new JSlider(MIN, MAX, RGB_MIN);
		bSliderMin = new JSlider(MIN, MAX, RGB_MIN);
		rSliderMax = new JSlider(MIN, MAX, RGB_MAX);
		gSliderMax = new JSlider(MIN, MAX, RGB_MAX);
		bSliderMax = new JSlider(MIN, MAX, RGB_MAX);

		rLabelMin = new JLabel("R_MIN:" + String.valueOf(RGB_MIN));
		gLabelMin = new JLabel("G_MIN:" + String.valueOf(RGB_MIN));
		bLabelMin = new JLabel("B_MIN:" + String.valueOf(RGB_MIN));
		rLabelMax = new JLabel("R_MAX:" + String.valueOf(RGB_MAX));
		gLabelMax = new JLabel("G_MAX:" + String.valueOf(RGB_MAX));
		bLabelMax = new JLabel("B_MAX:" + String.valueOf(RGB_MAX));

		addSliderListener();

		JSeparator sep = new JSeparator(JSeparator.HORIZONTAL);

		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		rgbAdjustPanel = new JPanel(gbl);
		rgbAdjustPanel.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1.0;
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbl.setConstraints(rSliderMin, gbc);
		rgbAdjustPanel.add(rSliderMin);

		gbc.insets = new Insets(4, 0, 0, 0);
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbl.setConstraints(gSliderMin, gbc);
		rgbAdjustPanel.add(gSliderMin);

		gbc.gridy = 2;
		gbl.setConstraints(bSliderMin, gbc);
		rgbAdjustPanel.add(bSliderMin);

		gbc.gridy = 3;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(4, 7, 0, 0);
		gbl.setConstraints(sep, gbc);
		rgbAdjustPanel.add(sep);

		gbc.gridy = 4;
		gbc.insets = new Insets(4, 0, 0, 0);
		gbc.gridwidth = 1;
		gbl.setConstraints(rSliderMax, gbc);
		rgbAdjustPanel.add(rSliderMax);

		gbc.gridy = 5;
		gbl.setConstraints(gSliderMax, gbc);
		rgbAdjustPanel.add(gSliderMax);

		gbc.gridy = 6;
		gbl.setConstraints(bSliderMax, gbc);
		rgbAdjustPanel.add(bSliderMax);

		gbc.insets = new Insets(0, 8, 0, 0);
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.weightx = 0.0;
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.fill = GridBagConstraints.NONE;
		gbl.setConstraints(rLabelMin, gbc);
		rgbAdjustPanel.add(rLabelMin);

		gbc.insets = new Insets(4, 8, 0, 0);
		gbc.gridy = 1;
		gbl.setConstraints(gLabelMin, gbc);
		rgbAdjustPanel.add(gLabelMin);

		gbc.gridy = 2;
		gbl.setConstraints(bLabelMin, gbc);
		rgbAdjustPanel.add(bLabelMin);

		gbc.gridy = 4;
		gbl.setConstraints(rLabelMax, gbc);
		rgbAdjustPanel.add(rLabelMax);

		gbc.gridy = 5;
		gbl.setConstraints(gLabelMax, gbc);
		rgbAdjustPanel.add(gLabelMax);

		gbc.gridy = 6;
		gbl.setConstraints(bLabelMax, gbc);
		rgbAdjustPanel.add(bLabelMax);

		speedTextLabel = new JLabel("Current speed: ");
		speedLabel = new JLabel("0.0 m/s");

		GridBagConstraints gbc2 = new GridBagConstraints();
		gbc2.gridx = 0;
		gbc2.gridy = 8;
		gbc2.insets = new Insets(4, 7, 0, 0);
		gbc2.fill = GridBagConstraints.NONE;
		gbc2.anchor = GridBagConstraints.LINE_START;
		gbc2.weightx = 0;
		gbl.setConstraints(speedTextLabel, gbc2);
		rgbAdjustPanel.add(speedTextLabel);

		gbc2.gridx = 0;
		gbc2.gridy = 9;
		gbc2.insets = new Insets(4, 7, 0, 0);
		gbc2.fill = GridBagConstraints.NONE;
		gbc2.anchor = GridBagConstraints.LINE_START;
		gbc2.weightx = 0;
		gbl.setConstraints(speedLabel, gbc2);
		rgbAdjustPanel.add(speedLabel);

		mainPanel.add(rgbAdjustPanel, BorderLayout.CENTER);
		rgbAdjustCanvasFrame.pack();
		rgbAdjustCanvasFrame.setSize(800, rgbAdjustCanvasFrame.getHeight());

		output.setLocation(0, 0);
		rgbAdjustCanvasFrame.setLocation(0, 520);

		// Initialize Sphero target movement parameter.
		spheroCoordinates = new CvPoint();

		kinectServer = new KinectServer();
		kinectServer.registerKinectServerCallBack(this);
	}

	private void addSliderListener()
	{
		rSliderMin.addChangeListener(new ChangeListener()
		{

			@Override
			public void stateChanged(ChangeEvent e)
			{
				rgba_min = cvScalar(rSliderMin.getValue(), gSliderMin.getValue(), bSliderMin.getValue(), 0);
				rLabelMin.setText("R_MIN:" + String.valueOf(rSliderMin.getValue()));
			}
		});

		gSliderMin.addChangeListener(new ChangeListener()
		{

			@Override
			public void stateChanged(ChangeEvent e)
			{
				rgba_min = cvScalar(rSliderMin.getValue(), gSliderMin.getValue(), bSliderMin.getValue(), 0);
				gLabelMin.setText("G_MIN:" + String.valueOf(gSliderMin.getValue()));
			}
		});

		bSliderMin.addChangeListener(new ChangeListener()
		{

			@Override
			public void stateChanged(ChangeEvent e)
			{
				rgba_min = cvScalar(rSliderMin.getValue(), gSliderMin.getValue(), bSliderMin.getValue(), 0);
				bLabelMin.setText("B_MIN:" + String.valueOf(bSliderMin.getValue()));
			}
		});

		rSliderMax.addChangeListener(new ChangeListener()
		{

			@Override
			public void stateChanged(ChangeEvent e)
			{
				if (rSliderMax.getValue() >= rSliderMin.getValue())
				{
					rgba_max = cvScalar(rSliderMax.getValue(), gSliderMax.getValue(), bSliderMax.getValue(), 0);
					rLabelMax.setText("R_MAX:" + String.valueOf(rSliderMax.getValue()));
				} else
					rSliderMax.setValue(rSliderMin.getValue());
			}
		});

		gSliderMax.addChangeListener(new ChangeListener()
		{

			@Override
			public void stateChanged(ChangeEvent e)
			{
				if (gSliderMax.getValue() >= gSliderMin.getValue())
				{
					rgba_max = cvScalar(rSliderMax.getValue(), gSliderMax.getValue(), bSliderMax.getValue(), 0);
					gLabelMax.setText("G_MAX:" + String.valueOf(gSliderMax.getValue()));
				} else
					gSliderMax.setValue(gSliderMin.getValue());
			}
		});

		bSliderMax.addChangeListener(new ChangeListener()
		{

			@Override
			public void stateChanged(ChangeEvent e)
			{
				if (bSliderMax.getValue() >= bSliderMin.getValue())
				{
					rgba_max = cvScalar(rSliderMax.getValue(), gSliderMax.getValue(), bSliderMax.getValue(), 0);
					bLabelMax.setText("B_MAX:" + String.valueOf(bSliderMax.getValue()));
				} else
					bSliderMax.setValue(bSliderMin.getValue());
			}
		});
	}

	private IplImage getThresholdImage(IplImage orgImg)
	{
		IplImage imgThreshold = cvCreateImage(cvGetSize(orgImg), 8, 1);
		cvInRangeS(orgImg, rgba_min, rgba_max, imgThreshold);// red
		cvSmooth(imgThreshold, imgThreshold, CV_MEDIAN, 15);
		return imgThreshold;
	}

	public IplImage Equalize(BufferedImage bufferedimg)
	{
		IplImage iploriginal = IplImage.createFrom(bufferedimg);
		IplImage srcimg = IplImage.create(iploriginal.width(), iploriginal.height(), IPL_DEPTH_8U, 1);
		IplImage destimg = IplImage.create(iploriginal.width(), iploriginal.height(), IPL_DEPTH_8U, 1);
		cvCvtColor(iploriginal, srcimg, CV_BGR2GRAY);
		cvEqualizeHist(srcimg, destimg);
		return destimg;
	}

	private IplImage getIplImage(int[] pixelArray, int width, int height)
	{
		IplImage imageDst;
		imageDst = IplImage.create(width, height, IPL_DEPTH_8U, 3);
		ByteBuffer imagePixels = imageDst.getByteBuffer();
		int locPImage, locIplImage, x, y;
		for (y = 0; y < 480; y++) {
			for (x = 0; x < 640; x++) {
				locPImage = x + y * 640;
				locIplImage = y * imageDst.widthStep() + 3 * x;
				imagePixels.put(locIplImage + 2, (byte) (pixelArray[locPImage] & 0xFF));
				imagePixels.put(locIplImage + 1, (byte) (pixelArray[locPImage] >> 8 & 0xFF));
				imagePixels.put(locIplImage, (byte) (pixelArray[locPImage] >> 16 & 0xFF));
			}
		}

		return imageDst;
	}

	/**
	 * Receive images from the kinect camera. This function handels the
	 * receiving of the video stream and initiatites the preprocessing od said
	 * stream. It has to choose the correct framegrabber, because opencv cannot
	 * access the kinect camera (at least we couldn't get it to work).
	 *
	 * @throws Exception
	 */
	private void receiveFromNetwork(int[] pixelArray, int width, int height) throws Exception
	{
		int posX = 0;
		int posY = 0;

		IplImage colorImage = getIplImage(pixelArray, width, height);

		if (colorImage != null)
		{
			// Convert the videostream from RGB to the openCV BGR format.
			cvCvtColor(colorImage, colorImage, CV_BGR2RGB);
			cvFlip(colorImage, colorImage, 1);

			videoFrame = colorImage;

			if (videoFrame == null)
			{
				System.out.printf("Video frame is null.\n");
				return;
			}

			IplImage detectThrs = getThresholdImage(colorImage);

			CvMoments moments = new CvMoments();
			cvMoments(detectThrs, moments, 1);

			double mom10 = cvGetSpatialMoment(moments, 1, 0);
			double mom01 = cvGetSpatialMoment(moments, 0, 1);
			double area = cvGetCentralMoment(moments, 0, 0);
			posX = (int) (mom10 / area);
			posY = (int) (mom01 / area);

			if (posX > 0 && posY > 0)
			{
				cvCircle(videoFrame, new CvPoint(posX, posY), 20, CvScalar.RED, -1, 8, 0);
			}

//			if (validateCoordinates(posX, posY))
//			{
				spheroCoordinates.put(posX, posY);
				String coordinates = String.valueOf(posX) + " " + String.valueOf(posY);
				kinectServer.sendStringContainer(new StringContainer(null, coordinates, null, null));
//			}
		}
	}

	private boolean validateCoordinates(int posX, int posY)
	{
		if (isInitial)
		{
			if (posX > 0 && posY > 0)
			{
				cvCircle(videoFrame, new CvPoint(posX, posY), 20, CvScalar.GREEN, -1, 8, 0);
			}

			int result = JOptionPane.showConfirmDialog(rgbAdjustCanvasFrame, "Is Sphero detected successfully?", "RoboView - SpheroTracking", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (result == JOptionPane.YES_OPTION)
			{
				isInitial = false;

				for (int i = 0; i < JITTER_BUFFER_LENGTH; i++)
				{
					jitterBufferX[i] = posX;
					jitterBufferY[i] = posY;
				}
			} else
				isInitial = true;

			return !isInitial;
		} else
		{
			int jitterX = 0;
			int jitterY = 0;

			for (int i = 0; i < JITTER_BUFFER_LENGTH; i++)
			{
				jitterX += jitterBufferX[i];
				jitterY += jitterBufferY[i];
			}

			jitterX = jitterX / JITTER_BUFFER_LENGTH;
			jitterY = jitterY / JITTER_BUFFER_LENGTH;

			checkBufferPosition();

			if ((posX > (jitterX + JITTER_TOLERANCE)) || (posX < (jitterX - JITTER_TOLERANCE)))
			{
				cvCircle(videoFrame, new CvPoint(jitterBufferX[bufferPosition], jitterBufferY[bufferPosition]), 20, CvScalar.GREEN, -1, 8, 0);
				return false;
			}
			else if ((posY > (jitterY + JITTER_TOLERANCE)) || (posY < (jitterY - JITTER_TOLERANCE)))
			{
				cvCircle(videoFrame, new CvPoint(jitterBufferX[bufferPosition], jitterBufferY[bufferPosition]), 20, CvScalar.GREEN, -1, 8, 0);
				return false;
			}
			else
			{
				if (posX > 0 && posY > 0)
				{
					cvCircle(videoFrame, new CvPoint(posX, posY), 20, CvScalar.GREEN, -1, 8, 0);
				}

				jitterBufferX[bufferPosition] = posX;
				jitterBufferY[bufferPosition] = posY;

				bufferPosition++;
			}
		}

		return true;
	}

	private void checkBufferPosition()
	{
		if(bufferPosition == JITTER_BUFFER_LENGTH)
			bufferPosition = 0;
	}

	/**
	 * Show a video of the used webcam. This function displays the captured
	 * images for show and tell.
	 */
	private void showVideo()
	{
		output.showImage(videoFrame);
	}

	/**
	 * Run of the mill entry point.
	 *
	 * @param args
	 *            No parameters are used in this program.
	 * @throws
	 */
	public static void main(String[] args) throws Exception
	{
		new SpheroTrackingNetwork();
	}

	@Override
	public void receivedAbsolutCoordinates(String coordiantes)
	{
		if (coordiantes!=null)
		{
			if (imageCounter==5)
			{
				oldPosition=currentPosition;
				currentPosition=coordiantes;
				imageCounter=0;
				calculateSpeed(oldPosition,currentPosition,oldTime,currentTime);
				return;
			}
			imageCounter++;
		}
	}

	// Berechnet aus zwei Absolutkoordinaten und deren Zeitpunkten die Geschwindigkeit
	private void calculateSpeed(String coords1, String coords2, long parTimestamp1, long parTimestamp2)
	{
		// Zerlegung der Koordinaten-Strings
		String[] tmpSplitCoords1 = coords1.split(" ");
		String[] tmpSplitCoords2 = coords2.split(" ");

		// Umwandlung der Einzel-Strings in Zahlen
		int pos1_x = Integer.parseInt(tmpSplitCoords1[0]);
		int pos1_y = Integer.parseInt(tmpSplitCoords1[1]);
		int pos2_x = Integer.parseInt(tmpSplitCoords2[0]);
		int pos2_y = Integer.parseInt(tmpSplitCoords2[1]);

		// Berechnung des Vektors zwischen den beiden Punkten
		int d_x = pos1_x;
		d_x -= pos2_x;
		int d_y = pos1_y;
		d_y -= pos2_y;

		// Berechnung der Laenge des Distanzvektors
		double distance = Math.pow(d_x, 2);
		distance += Math.pow(d_y, 2);
		distance = Math.sqrt(distance);
		distance /= 10;

		// Berechnung des Zeitunterschieds
		double time = parTimestamp2;
		time -= parTimestamp1;
		time /= 1000;

		double speed = distance;
		speed /= time;

		kinectServer.sendStringContainer(new StringContainer(new Double(speed).toString(),null,null,null));
		System.out.println("Speed: "+speed+" cm/s");
	}

	@Override
	public void receivedTimestamp(long timestamp)
	{
		if (imageCounter==5)
		{
			oldTime=currentTime;
			currentTime=timestamp;
			imageCounter=0;
			return;
		}
		imageCounter++;

	}

	@Override
	public void receivedImageIntegerArray(int[] integerArray, int width, int height)
	{
		try
		{
			receiveFromNetwork(integerArray, width, height);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		showVideo();
	}

}
