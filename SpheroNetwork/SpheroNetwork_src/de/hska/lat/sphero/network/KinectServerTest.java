package de.hska.lat.sphero.network;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JFrame;

import de.hska.lat.sphero.network.impl.KinectServer;
import de.hska.lat.sphero.network.impl.objects.StringContainer;

public class KinectServerTest implements IKinectServerCallBack
{

	private KinectServer kinectServer;

	public KinectServerTest()
	{
		kinectServer = new KinectServer();
		kinectServer.registerKinectServerCallBack(this);

		JFrame frame = new JFrame("KinectServerTest");
		frame.setLayout(new BorderLayout());
		JButton button = new JButton("Send KinectDataContainer");
		button.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				send();
			}
		});
		frame.add(button, BorderLayout.CENTER);
		frame.setSize(100, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}

	// 07216909164
	private void send()
	{
		kinectServer.sendStringContainer(new StringContainer("1", "2", "3", "4"));
	}

	public static void main(String[] args)
	{
		new KinectServerTest();
	}

	@Override
	public void receivedAbsolutCoordinates(String coordiantes)
	{
		System.out.println("receivedAbsolutCoordinates");
	}

	@Override
	public void receivedTimestamp(long timestamp)
	{
		System.out.println("receivedTimestamp");

	}

	@Override
	public void receivedImageIntegerArray(int[] integerArray, int width, int height)
	{
		System.out.println("receivedImageIntegerArray");
	}

}
