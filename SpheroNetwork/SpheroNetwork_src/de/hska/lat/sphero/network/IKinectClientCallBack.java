package de.hska.lat.sphero.network;


public interface IKinectClientCallBack
{
	public abstract void receivedSpheroPixelCoordinates(String coordinates);
	public abstract void receivedSpheroAbsolutCoordinates(String coordinates);
	public abstract void receivedSpheroSpeed(String speed);
	public abstract void receivedSpheroTargetCoordinates(String coordinates);
}
