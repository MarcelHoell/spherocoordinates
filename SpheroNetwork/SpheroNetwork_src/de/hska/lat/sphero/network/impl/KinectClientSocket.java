package de.hska.lat.sphero.network.impl;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public class KinectClientSocket
{
	private Socket socket;
	private InputStream input;
	private OutputStream output;
	private ObjectInputStream objectInputStream;
	private ObjectOutputStream objectOutputStream;

	public KinectClientSocket(Socket socket, InputStream input, OutputStream output, ObjectInputStream objectInputStream, ObjectOutputStream objectOutputStream)
	{
		this.socket = socket;
		this.input = input;
		this.output = output;
		this.objectInputStream = objectInputStream;
		this.objectOutputStream = objectOutputStream;
	}

	public Socket getSocket()
	{
		return socket;
	}

	public void setSocket(Socket socket)
	{
		this.socket = socket;
	}

	public InputStream getInputStream()
	{
		return input;
	}

	public void setInputStream(InputStream input)
	{
		this.input = input;
	}

	public OutputStream getOutputStream()
	{
		return output;
	}

	public void setOutputStream(OutputStream output)
	{
		this.output = output;
	}

	public ObjectInputStream getObjectInputStream()
	{
		return objectInputStream;
	}

	public void setObjectInputStream(ObjectInputStream objectInputStream)
	{
		this.objectInputStream = objectInputStream;
	}

	public ObjectOutputStream getObjectOutputStream()
	{
		return objectOutputStream;
	}

	public void setObjectOutputStream(ObjectOutputStream objectOutputStream)
	{
		this.objectOutputStream = objectOutputStream;
	}
}
