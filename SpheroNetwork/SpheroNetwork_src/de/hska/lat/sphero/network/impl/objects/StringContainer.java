package de.hska.lat.sphero.network.impl.objects;

import java.io.Serializable;

public class StringContainer implements Serializable
{
	/**
	 *
	 */
	private static final long serialVersionUID = 8754418962865636258L;
	private String spheroSpeed;
	private String spheroPixelCoordinates;
	private String spheroAbsolutCoordinates;
	private String spheroTargetCoordinates;

	public StringContainer(String spheroSpeed, String spheroPixelCoordinates, String spheroAbsolutCoordinates, String spheroTargetCoordinates)
	{
		this.spheroSpeed = spheroSpeed;
		this.spheroPixelCoordinates = spheroPixelCoordinates;
		this.spheroAbsolutCoordinates = spheroAbsolutCoordinates;
		this.spheroTargetCoordinates = spheroTargetCoordinates;
	}

	public String getSpheroSpeed()
	{
		return spheroSpeed;
	}

	public String getSpheroPixelCoordinates()
	{
		return spheroPixelCoordinates;
	}

	public String getSpheroAbsolutCoordinates()
	{
		return spheroAbsolutCoordinates;
	}

	public String getSpheroTargetCoordinates()
	{
		return spheroTargetCoordinates;
	}

	public void setSpheroSpeed(String spheroSpeed)
	{
		this.spheroSpeed = spheroSpeed;
	}

	public void setSpheroPixelCoordinates(String spheroPixelCoordinates)
	{
		this.spheroPixelCoordinates = spheroPixelCoordinates;
	}

	public void setSpheroAbsolutCoordinates(String spheroAbsolutCoordinates)
	{
		this.spheroAbsolutCoordinates = spheroAbsolutCoordinates;
	}

	public void setSpheroTargetCoordinates(String spheroTargetCoordinates)
	{
		this.spheroTargetCoordinates = spheroTargetCoordinates;
	}
}
