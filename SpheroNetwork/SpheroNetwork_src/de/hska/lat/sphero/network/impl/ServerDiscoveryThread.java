package de.hska.lat.sphero.network.impl;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ServerDiscoveryThread implements Runnable
{

	@Override
	public void run()
	{
		DatagramSocket socket;
		try
		{
			// Keep a socket open to listen to all the UDP trafic that is
			// destined for this port
			socket = new DatagramSocket(KinectServer.SERVER_PORT_1337, InetAddress.getByName("0.0.0.0"));
			socket.setBroadcast(true);

			while (true)
			{
				System.out.println("<><><> Ready to receive broadcast packets!");

				// Receive a packet
				byte[] recvBuf = new byte[15000];
				DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
				socket.receive(packet);

				// Packet received
				System.out.println("<<< Discovery packet received from: " + packet.getAddress().getHostAddress());
				System.out.println("<<< Packet received; data: " + new String(packet.getData()));

				// See if the packet holds the right command (message)
				String message = new String(packet.getData()).trim();
				if (message.equals("DISCOVER_FUIFSERVER_REQUEST"))
				{
					byte[] sendData = "DISCOVER_FUIFSERVER_RESPONSE".getBytes();

					// Send a response
					DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, packet.getAddress(), packet.getPort());
					socket.send(sendPacket);

					System.out.println(">>> Sent packet to: " + sendPacket.getAddress().getHostAddress());
				}
			}
		} catch (IOException ex)
		{
			ex.printStackTrace();
		}
	}

	public static ServerDiscoveryThread getInstance()
	{
		return ServerDiscoveryThreadHolder.INSTANCE;
	}

	private static class ServerDiscoveryThreadHolder
	{
		private static final ServerDiscoveryThread INSTANCE = new ServerDiscoveryThread();
	}
}
