package de.hska.lat.sphero.network.impl.objects;

import java.io.Serializable;

public class KinectDataContainer implements Serializable
{
	/**
	 * Unique version ID to identify object.
	 */
	private static final long serialVersionUID = -2999142551855463838L;
	private int[] integerArray;
	private long timestamp;
	private int height;
	private int width;

	/**
	 * Constructor to create a new object containing data for image and it's timestamp.
	 * @param integerArray Array with integers containing BGR values for image.
	 * @param timestamp Timestamp when picture was taken
	 * @param height Height of the resulting image
	 * @param width Width of the resulting image
	 */
	public KinectDataContainer(int[] integerArray, long timestamp, int height, int width)
	{
		this.integerArray = integerArray;
		this.timestamp = timestamp;
		this.height = height;
		this.width = width;
	}

	public int[] getIntegerArray()
	{
		return integerArray;
	}

	public long getTimestamp()
	{
		return timestamp;
	}

	public int getHeight()
	{
		return height;
	}

	public int getWidth()
	{
		return width;
	}

	public void setIntegerArray(int[] integerArray)
	{
		this.integerArray = integerArray;
	}

	public void setTimestamp(long timestamp)
	{
		this.timestamp = timestamp;
	}

	public void setHeight(int height)
	{
		this.height = height;
	}

	public void setWidth(int width)
	{
		this.width = width;
	}
}
