package de.hska.lat.sphero.network.impl;

import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.util.Enumeration;

import javax.imageio.ImageIO;

import de.hska.lat.sphero.network.IKinectClientCallBack;
import de.hska.lat.sphero.network.impl.objects.KinectDataContainer;
import de.hska.lat.sphero.network.impl.objects.StringContainer;

public class KinectClient
{
	private InetAddress serverAddress = null;
	private Socket socket1337;
	private Socket socket4711;
	private InputStream inputStream1337;
	private OutputStream outputStream1337;
	private ObjectInputStream objectInputStream1337;
	private ObjectOutputStream objectOutputStream1337;
	private InputStream inputStream4711;
	private OutputStream outputStream4711;
	private ObjectInputStream objectInputStream4711;
	private ObjectOutputStream objectOutputStream4711;
	private IKinectClientCallBack clientCallBack;

	public KinectClient()
	{
		startServerDiscovery();
	}

	private void startServerDiscovery()
	{
		DatagramSocket socket;

		// Find the server using UDP broadcast
		try
		{
			// Open a random port to send the package
			socket = new DatagramSocket();
			socket.setBroadcast(true);

			byte[] sendData = "DISCOVER_FUIFSERVER_REQUEST".getBytes();

			// Try the 255.255.255.255 first
			try
			{
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName("255.255.255.255"), KinectServer.SERVER_PORT_1337);
				socket.send(sendPacket);
				System.out.println(">>> Request packet sent to: 255.255.255.255 (DEFAULT)");
			} catch (Exception e)
			{
				e.printStackTrace();
			}

			// Broadcast the message over all the network interfaces
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements())
			{
				NetworkInterface networkInterface = (NetworkInterface) interfaces.nextElement();

				if (networkInterface.isLoopback() || !networkInterface.isUp())
				{
					continue; // Don't want to broadcast to the loopback
								// interface
				}

				for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses())
				{
					InetAddress broadcast = interfaceAddress.getBroadcast();
					if (broadcast == null)
					{
						continue;
					}

					// Send the broadcast package!
					try
					{
						DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, broadcast, KinectServer.SERVER_PORT_1337);
						socket.send(sendPacket);
					} catch (Exception e)
					{
					}

					System.out.println(">>> Request packet sent to: " + broadcast.getHostAddress() + "; Interface: " + networkInterface.getDisplayName());
				}
			}

			System.out.println(">>> Done looping over all network interfaces. Now waiting for a reply!");

			// Wait for a response
			byte[] recvBuf = new byte[15000];
			DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);
			socket.receive(receivePacket);

			// We have a response
			System.out.println("<<< Broadcast response from server: " + receivePacket.getAddress().getHostAddress());

			// Check if the message is correct
			String message = new String(receivePacket.getData()).trim();
			if (message.equals("DISCOVER_FUIFSERVER_RESPONSE"))
			{
				// DO SOMETHING WITH THE SERVER'S IP (for example, store
				// it in
				// your controller)
				serverAddress = receivePacket.getAddress();
				System.out.println("<><><> Server address is: " + serverAddress.getHostAddress());
			}

			// Close the port!
			socket.close();
		} catch (IOException ex)
		{
			ex.printStackTrace();
		}

		connectToServer();
	}

	public void registerKinectClientCallBack(IKinectClientCallBack clientCallBack)
	{
		this.clientCallBack = clientCallBack;
	}

	public void sendStringContainer(StringContainer stringContainer)
	{
		try
		{
			if (!checkConnectionClosed())
			{
				if (stringContainer != null)
				{
					if (objectOutputStream4711 != null)
						objectOutputStream4711.writeObject(stringContainer);
				}
			}
		} catch (IOException e)
		{
			System.out.println("Can not get ObjectOutputStream for server on port 4711: \"" + serverAddress + "\".");
		}
	}

	public void sendKinectDataContainer(KinectDataContainer kinectDataContainer)
	{
		try
		{
			if (!checkConnectionClosed())
			{
				if (kinectDataContainer != null)
				{
					if (objectOutputStream1337 != null)
					{
						objectOutputStream1337.writeObject(kinectDataContainer);
//						objectInputStream1337.reset();
//						System.gc();
					}
				}
			}
		} catch (IOException e)
		{
			System.out.println("Can not get ObjectOutputStream for server on port 1337: \"" + serverAddress + "\".");
		}
	}

//	public void sendIntegerArray(Integer[] integerArray)
//	{
//		try
//		{
//			if (!checkConnectionClosed())
//			{
//				if (integerArray != null)
//				{
//					if (objectOutputStream1337 != null)
//						objectOutputStream1337.writeObject(integerArray);
//				}
//			}
//		} catch (IOException e)
//		{
//			System.out.println("Can not get ObjectOutputStream for server on port 4711: \"" + serverAddress + "\".");
//		}
//	}
//
//	public void sendBufferedImage(BufferedImage bufferedImage)
//	{
//		try
//		{
//			if (!checkConnectionClosed())
//			{
//				if(bufferedImage != null)
//				{
//					if (objectOutputStream1337 != null)
//					{
//						ImageIO.write(bufferedImage, "NONE", objectOutputStream1337);
//					}
//				}
//			}
//		} catch (IOException e)
//		{
//			System.out.println("Can not get ObjectOutputStream for server on port 1337: \"" + serverAddress + "\".");
//		}
//	}
//
//	public void sendTimestamp(long timestamp)
//	{
//		try
//		{
//			if (!checkConnectionClosed())
//			{
//				if (objectOutputStream1337 != null)
//				{
//					DataOutputStream dos = new DataOutputStream(objectOutputStream1337);
//					dos.writeLong(timestamp);
//				}
//			}
//		} catch (IOException e)
//		{
//			System.out.println("Can not get ObjectOutputStream for server on port 1337: \"" + serverAddress + "\".");
//		}
//	}

	private void receiveStringContainer()
	{
		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				while (true)
				{
					try
					{
						if (!checkConnectionClosed())
						{
							if (objectInputStream4711 != null)
							{
								StringContainer stringContainer = (StringContainer) objectInputStream4711.readObject();
								if (clientCallBack != null)
								{
									clientCallBack.receivedSpheroAbsolutCoordinates(stringContainer.getSpheroAbsolutCoordinates());
									clientCallBack.receivedSpheroPixelCoordinates(stringContainer.getSpheroPixelCoordinates());
									clientCallBack.receivedSpheroSpeed(stringContainer.getSpheroSpeed());
									clientCallBack.receivedSpheroTargetCoordinates(stringContainer.getSpheroTargetCoordinates());
								}
							}
						} else
							break;
					} catch (ClassNotFoundException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();

					} catch (IOException e)
					{
						System.out.println("Connection closed by server. Exiting...");
						try
						{
							if (socket1337 != null)
								socket1337.close();
						} catch (IOException e1)
						{
							System.out.println("Unable to close socket.");
						}
						System.exit(0);
					}
				}
			}
		}).start();
	}

	private boolean checkConnectionClosed()
	{
		if (socket1337.isClosed())
			return true;
		return false;
	}

	private void connectToServer()
	{
		if (serverAddress != null)
		{
			try
			{
				socket1337 = new Socket(serverAddress, KinectServer.SERVER_PORT_1337);
				inputStream1337 = socket1337.getInputStream();
				outputStream1337 = socket1337.getOutputStream();
				objectInputStream1337 = new ObjectInputStream(inputStream1337);
				objectOutputStream1337 = new ObjectOutputStream(outputStream1337);

				socket4711 = new Socket(serverAddress, KinectServer.SERVER_PORT_4711);
				inputStream4711 = socket4711.getInputStream();
				outputStream4711 = socket4711.getOutputStream();
				objectInputStream4711 = new ObjectInputStream(inputStream4711);
				objectOutputStream4711 = new ObjectOutputStream(outputStream4711);

				receiveStringContainer();
			} catch (IOException e)
			{
				System.out.println("Client can not connect to server.");
			}
		}
	}
}
