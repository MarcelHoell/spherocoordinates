package de.hska.lat.sphero.network.impl;

import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import de.hska.lat.sphero.network.IKinectServerCallBack;
import de.hska.lat.sphero.network.impl.objects.KinectDataContainer;
import de.hska.lat.sphero.network.impl.objects.StringContainer;

public class KinectServer
{
	public static final int SERVER_PORT_1337 = 1337;
	public static final int SERVER_PORT_4711 = 4711;
	private ServerSocket serverSocket1337;
	private ServerSocket serverSocket4711;
	private List<KinectClientSocket> kinectClientSocketList1337;
	private List<KinectClientSocket> kinectClientSocketList4711;
	private IKinectServerCallBack serverCallBack;

	public KinectServer()
	{
		try
		{
			Thread discoveryThread = new Thread(ServerDiscoveryThread.getInstance());
			discoveryThread.start();
			serverSocket1337 = new ServerSocket(SERVER_PORT_1337);
			serverSocket4711 = new ServerSocket(SERVER_PORT_4711);
			System.out.println("Server started.");
			connectClient();
		} catch (IOException e)
		{
			System.out.println("Can not establish SERVER_PORT.");
		}
	}

	private void connectClient()
	{
		kinectClientSocketList1337 = new ArrayList<>();
		kinectClientSocketList4711 = new ArrayList<>();
		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				Socket clientSocket = null;
				while (true)
				{
					try
					{
						clientSocket = serverSocket1337.accept();
						InputStream input = clientSocket.getInputStream();
						OutputStream output = clientSocket.getOutputStream();
						ObjectOutputStream objectOutputStream = new ObjectOutputStream(output);
						ObjectInputStream objectInputStream = new ObjectInputStream(input);
						KinectClientSocket temp = new KinectClientSocket(clientSocket, input, output, objectInputStream, objectOutputStream);

						kinectClientSocketList1337.add(temp);
						receiveKinectDataContainer(temp);

						System.out.println("Client successfully connected on port 1337: \"" + clientSocket.getInetAddress() + "\".");
					} catch (IOException e)
					{
						if (clientSocket != null)
							System.out.println("Client can not be connected on port 1337: \"" + clientSocket.getInetAddress() + "\".");
						else
							System.out.println("Client can not be connected on port 1337.");
					}
				}
			}
		}).start();

		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				Socket clientSocket = null;
				while (true)
				{
					try
					{
						clientSocket = serverSocket4711.accept();
						InputStream input = clientSocket.getInputStream();
						OutputStream output = clientSocket.getOutputStream();
						ObjectOutputStream objectOutputStream = new ObjectOutputStream(output);
						ObjectInputStream objectInputStream = new ObjectInputStream(input);
						KinectClientSocket temp = new KinectClientSocket(clientSocket, input, output, objectInputStream, objectOutputStream);

						kinectClientSocketList4711.add(temp);
						receiveStringContainer(temp);

						System.out.println("Client successfully connected on port 4711: \"" + clientSocket.getInetAddress() + "\".");
					} catch (IOException e)
					{
						if (clientSocket != null)
							System.out.println("Client can not be connected on port 4711: \"" + clientSocket.getInetAddress() + "\".");
						else
							System.out.println("Client can not be connected on port 4711.");
					}
				}
			}
		}).start();
	}

	public void registerKinectServerCallBack(IKinectServerCallBack serverCallBack)
	{
		this.serverCallBack = serverCallBack;
	}

	public void sendStringContainer(StringContainer stringContainer)
	{
		for (int i = 0; i < kinectClientSocketList4711.size(); i++)
		{
			if (!checkConnectionClosed(kinectClientSocketList4711.get(i)))
			{
				if (kinectClientSocketList4711.get(i).getObjectOutputStream() != null)
				{
					try
					{
						kinectClientSocketList4711.get(i).getObjectOutputStream().writeObject(stringContainer);
					} catch (IOException e)
					{
						System.out.println("Problem with sending StringContainer.");
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void sendStringContainer(StringContainer stringContainer, KinectClientSocket kinectClientSocket)
	{
		for (int i = 0; i < kinectClientSocketList4711.size(); i++)
		{
			if (!checkConnectionClosed(kinectClientSocketList4711.get(i)))
			{
				if (!kinectClientSocketList4711.get(i).equals(kinectClientSocket))
				{
					if (kinectClientSocketList4711.get(i).getObjectOutputStream() != null)
					{
						try
						{
							kinectClientSocketList4711.get(i).getObjectOutputStream().writeObject(stringContainer);
						} catch (IOException e)
						{
							System.out.println("Problem with sending StringContainer.");
							e.printStackTrace();
						}
					}
				}
			}
		}

	}

	private void receiveStringContainer(final KinectClientSocket kinectClientSocket)
	{
		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				StringContainer stringContainer;
				while (true)
				{
					try
					{
						if (!checkConnectionClosed(kinectClientSocket))
						{
							if (kinectClientSocket.getObjectInputStream() != null)
							{
								stringContainer = (StringContainer) kinectClientSocket.getObjectInputStream().readObject();
								if (serverCallBack != null)
								{
									serverCallBack.receivedAbsolutCoordinates(stringContainer.getSpheroAbsolutCoordinates());
									sendStringContainer(stringContainer, kinectClientSocket);
								}
							}
						} else
							break;
					} catch (ClassNotFoundException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e)
					{
						System.out.println("Connection closed by client. Exiting...");
						try
						{
							if (kinectClientSocket.getSocket() != null)
								kinectClientSocket.getSocket().close();
						} catch (IOException e1)
						{
							System.out.println("Unable to close socket.");
						}
						break;
					}
				}
			}
		}).start();
	}

	private void receiveKinectDataContainer(final KinectClientSocket kinectClientSocket)
	{
		new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				KinectDataContainer kinectDataContainer;
				while (true)
				{
					try
					{
						if (!checkConnectionClosed(kinectClientSocket))
						{
							if (kinectClientSocket.getObjectInputStream() != null)
							{
								kinectDataContainer = (KinectDataContainer)kinectClientSocket.getObjectInputStream().readObject();

								if (serverCallBack != null)
								{
									serverCallBack.receivedImageIntegerArray(kinectDataContainer.getIntegerArray(), kinectDataContainer.getWidth(), kinectDataContainer.getHeight());
									serverCallBack.receivedTimestamp(kinectDataContainer.getTimestamp());
								}
							}
						} else
							break;
					} catch (IOException e)
					{
						System.out.println("Connection closed by client. Exiting...");
						e.printStackTrace();
						try
						{
							if (kinectClientSocket.getSocket() != null)
								kinectClientSocket.getSocket().close();
						} catch (IOException e1)
						{
							System.out.println("Unable to close socket.");
						}
						break;
					} catch (ClassNotFoundException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	private boolean checkConnectionClosed(KinectClientSocket kinectClientSocket)
	{
		if (kinectClientSocket.getSocket().isClosed())
			return true;
		return false;
	}
}
