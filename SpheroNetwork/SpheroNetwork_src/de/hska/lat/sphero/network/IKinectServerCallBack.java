package de.hska.lat.sphero.network;

import java.awt.image.BufferedImage;

public interface IKinectServerCallBack
{
	public abstract void receivedTimestamp(long timestamp);
	public abstract void receivedAbsolutCoordinates(String coordiantes);
	public abstract void receivedImageIntegerArray(int[] integerArray, int width, int height);
}
