package de.hska.lat.sphero.network;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JFrame;

import de.hska.lat.sphero.network.impl.KinectClient;
import de.hska.lat.sphero.network.impl.objects.KinectDataContainer;

public class KinectClientTest implements IKinectClientCallBack
{
	private KinectClient kinectClient;

	public KinectClientTest()
	{
		kinectClient = new KinectClient();
		kinectClient.registerKinectClientCallBack(this);

		JFrame frame = new JFrame("KinectClientTest");
		frame.setLayout(new BorderLayout());
		JButton button = new JButton("Send GroundLayer Container");
		button.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				send();
			}
		});
		frame.add(button, BorderLayout.CENTER);
		frame.setSize(100, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}

	private void send()
	{
		int[] test = {2, 2, 3, 4};
		kinectClient.sendKinectDataContainer(new KinectDataContainer(test, 5l, 50, 100));
	}

	public static void main(String[] args)
	{
		new KinectClientTest();
	}

	@Override
	public void receivedSpheroPixelCoordinates(String coordinates)
	{
		System.out.println("receivedSpheroPixelCoordinates");
	}

	@Override
	public void receivedSpheroAbsolutCoordinates(String coordinates)
	{
		System.out.println("receivedSpheroAbsolutCoordinates");
	}

	@Override
	public void receivedSpheroSpeed(String speed)
	{
		System.out.println("receivedSpheroSpeed");
	}

	@Override
	public void receivedSpheroTargetCoordinates(String coordinates)
	{
		System.out.println("receivedSpheroTargetCoordinates");
	}
}
